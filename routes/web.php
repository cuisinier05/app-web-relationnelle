<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Livewire\Home;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

/* ---------------------------------------------------FRONT-END-------------------------------------------*/
// Route to verify user and show homepage in front-end
Route::middleware(['auth:sanctum', 'verified'])->get('/', function () {
    return view('home');
})->name('home');

// Route to verify user and show categories in front-end
Route::middleware(['auth:sanctum', 'verified'])->get('/categories', function () {
    return view('categories');
})->name('categories');

// Route to verify user and show ressources in front-end
Route::middleware(['auth:sanctum', 'verified'])->get('/ressources', function () {
    return view('ressources');
})->name('ressources');
/* ---------------------------------------------------COMPTE UTILISATEUR-------------------------------------------*/
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

/* ---------------------------------------------------BACK-END-------------------------------------------*/
// Route to show users in back-end
Route::get('/users', [UserController::class, 'index'])->name('users.index');



