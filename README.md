# Application web [Re]sources relationnelles
## La plateforme pour améliorer vos relations
### Description :
Cette plateforme a pour but le partage de ressources entre utilisateurs. Chaque utilisateur pourra se créer son équipe avec d'autres utilisateurs de la plateforme afin de discuter et de partager au maximum.
Le but étant de se rapprocher les uns des autres dans un but commun.

## Stack de l'application :
`"php": "^7.3|^8.0",`
        `"bensampo/laravel-enum": "^3.2",`
        `"fideloper/proxy": "^4.4",`
        `"fruitcake/laravel-cors": "^2.0",`
        `"guzzlehttp/guzzle": "^7.0.1",`
        `"laravel/framework": "^8.12",`
        `"laravel/jetstream": "^2.0",`
        `"laravel/sanctum": "^2.6",`
        `"laravel/tinker": "^2.5",`
        `"livewire/livewire": "^2.0",`
        `"yajra/laravel-datatables-oracle": "^9.15"`

`"devDependencies": {`
        `"@tailwindcss/forms": "^0.2.1",`
        `"@tailwindcss/typography": "^0.3.0",`
        `"alpinejs": "^2.7.3",`
        `"autoprefixer": "^10.0.2",`
        `"axios": "^0.21",`
        `"laravel-mix": "^6.0.10",`
        `"lodash": "^4.17.19",`
        `"postcss": "^8.2.4",`
        `"postcss-import": "^12.0.1",`
        `"resolve-url-loader": "^3.1.2",`
        `"sass": "^1.32.4",`
        `"sass-loader": "^7.1.0",`
        `"tailwindcss": "^2.0.1"`
    }`

## Installation :
1) Cloner le projet en local
2) `composer install && npm install`
3) Editez votre fichier .env
4) `php artisan migrate`
5) `php artisan serve`

ENJOY !!!
