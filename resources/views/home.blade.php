<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/home.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Accueil</title>
</head>
<body>
    <!-- Début de la partie 1 -->
    <header>
        <div class="flex part1">
            <img class="ml-10 mt-4" id="logo-home" src="{{ asset('../../img/logo-resources-relationnelles.PNG') }}" alt="Logo"/>
            <nav class="flex-grow mt-10">
                <ul class="flex justify-around w-600">
                    <li class="m-10"><a href="#">Item1</a></li>
                    <li class="m-10"><a href="#">Item2</a></li>
                    <li class="m-10"><a href="#">Item3</a></li>
                    <li class="m-10"><a href="#">Item4</a></li>
                    <li class="m-10"><a href="#">Item5</a></li>
                    <li class="m-10"><a href="#">A propos</a></li>
                    <li class="m-10"><a href="/dashboard">Mon compte</a></li>
                    <li class="m-8"><a href="/login"><button class="btn btn-primary" type="submit" >Log'In</button></a></li>
                    <li class="m-8"><a href="/register"><button class="btn btn-primary" type="submit" >Sign'In</button></a></li>
                </ul>
            </nav>
        </div>
    </header>
    <!-- Fin de la partie 1 -->

    <!-- Début de la partie 2 -->
    <div class="mt-10 ">
        <img src="https://picsum.photos/1600/500" alt="image d'entête">
        <h1 class="entete-homepage">[Re]sources relationnelles</h1>
        <h2 class="entete-homepage sous-titre">La plateforme pour améliorer vos relations</h2>
        <form class="d-flex recherche">
            <input class="form-control me-2 " type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success search-button" type="submit">Search</button>
        </form>
    </div>
    <!-- Fin de la partie 2 -->

    <!-- Début de la partie 3 -->
    <div class="content part3">
        <h2>Catégories des ressources</h2>
        <div class="flex justify-around m-10">    
            <div class="item-categorie">
                <img src="https://picsum.photos/80" alt="photo catégorie">
                <p>Category name</p>
            </div>
            <div class="item-categorie">
                <img src="https://picsum.photos/80" alt="photo catégorie">
                <p>Category name</p>
            </div>
            <div class="item-categorie">
                <img src="https://picsum.photos/80" alt="photo catégorie">
                <p>Category name</p>
            </div>
            <div class="item-categorie">
                <img src="https://picsum.photos/80" alt="photo catégorie">
                <p>Category name</p>
            </div class="item-categorie">
            <div class="item-categorie">
                <img src="https://picsum.photos/80" alt="photo catégorie">
                <p>Category name</p>
            </div>
            <div class="item-categorie">
                <img src="https://picsum.photos/80" alt="photo catégorie">
                <p>Category name</p>
            </div class="item-categorie">
        </div>
        <a href="/categories">
            <button class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150 see-more-button">En voir plus !
            </button>
        </a>
    </div>
    <!-- Fin de la partie 3 -->
    <hr>
    <!-- Début de la partie 4 -->
    <div class="content part4">
        <h2>Ressources les plus populaires</h2>
        <div class="flex justify-around m-10">    
            <div class="item-categorie">
                <img src="https://picsum.photos/80" alt="photo catégorie">
                <p>Resource name</p>
            </div>
            <div class="item-categorie">
                <img src="https://picsum.photos/80" alt="photo catégorie">
                <p>Resource name</p>
            </div>
            <div class="item-categorie">
                <img src="https://picsum.photos/80" alt="photo catégorie">
                <p>Resource name</p>
            </div>
            <div class="item-categorie">
                <img src="https://picsum.photos/80" alt="photo catégorie">
                <p>Resource name</p>
            </div class="item-categorie">
        </div>
        <a href="/ressources">
            <button class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150 see-more-button">En voir plus !
            </button>
        </a>
    </div>
    <!-- Fin de la partie 4 -->
    <hr>

    <!-- Début de la partie 5 -->
    <div class="content part5">
        <h2>[Re]sourceurs les plus en vogue</h2>
        <div class="flex justify-between m-10">    
            <div class="item-categorie">
                <img src="https://picsum.photos/80" alt="photo catégorie">
                <p>[Re]sourceurs name</p>
            </div>
            <div class="item-categorie">
                <img src="https://picsum.photos/80" alt="photo catégorie">
                <p>[Re]sourceurs name</p>
            </div>
            <div class="item-categorie">
                <img src="https://picsum.photos/80" alt="photo catégorie">
                <p>[Re]sourceurs name</p>
            </div>
            <div class="item-categorie">
                <img src="https://picsum.photos/80" alt="photo catégorie">
                <p>[Re]sourceurs name</p>
            </div class="item-categorie">
        </div>
    </div>
    <!-- Fin de la partie 5 -->

    <footer>

    </footer>
</body>
</html>